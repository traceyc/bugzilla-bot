Thanks for your comment!

Automatically switching the status of this bug to REPORTED so that the KDE team
knows that the bug is ready to get confirmed.

In the future you may also do this yourself when providing needed information.
