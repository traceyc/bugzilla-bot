# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

# Comment templates.
class Template
  REMIND = :remind
  CLOSE = :close
  REVERT = :revert

  TEMPLATES = {
    REMIND => 'remind.txt',
    CLOSE => 'close.txt',
    REVERT => 'revert.txt'
  }.freeze

  attr_reader :data
  alias to_s data

  def initialize(type)
    file = TEMPLATES[type]
    raise "Invalid type #{type}" unless file
    @data = File.read(File.join(__dir__, '../data', file))
  end

  class << self
    def remind
      Template.new(REMIND)
    end

    def close
      Template.new(CLOSE)
    end

    def revert
      Template.new(REVERT)
    end
  end
end
