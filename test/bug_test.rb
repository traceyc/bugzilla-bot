# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

require 'test_helper'

class BugTest < Minitest::Test
  # Bug doesn't really contain anything worthwhile testing beyond code coverage
  # as the entire class is an API facade.
end
