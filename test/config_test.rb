# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

require 'test_helper'

class ConfigTest < Minitest::Test
  def test_path
    assert_includes(Bugzillabot::Config.new.path, __dir__)
  end
end
